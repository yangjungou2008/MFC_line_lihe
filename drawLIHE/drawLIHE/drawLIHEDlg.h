// drawLIHEDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CdrawLIHEDlg dialog
class CdrawLIHEDlg : public CDialog
{
// Construction
public:
	CdrawLIHEDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_DRAWLIHE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_pic;
	void DrawPoint(double x, double y);
	void DrawLine(double xstart, double ystart, double xend, double yend);
	BOOL CalculateLineKB(double *x,double *y,int n,double &k,double &b);
	void DrawLineFx(double k ,double b);
	void ClearPic();
	afx_msg void OnBnClickedDrawpoint();
	afx_msg void OnBnClickedPointline();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedPointfit();
};
