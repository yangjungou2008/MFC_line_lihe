// drawLIHEDlg.cpp : implementation file
//

#include "stdafx.h"
#include "drawLIHE.h"
#include "drawLIHEDlg.h"
#include "math.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

int ExpN = 10; 
double Xmax = 100;
double Ymax = 100;
double Xdata[10] = {0,12,23,30,54,65,77,82,86,94};
double Ydata[10] = {0,12,52,64,54,65,77,82,86,94};
int DataCount = 10 ;

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CdrawLIHEDlg dialog




CdrawLIHEDlg::CdrawLIHEDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CdrawLIHEDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CdrawLIHEDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PIC, m_pic);
}

BEGIN_MESSAGE_MAP(CdrawLIHEDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_DRAWPOINT, &CdrawLIHEDlg::OnBnClickedDrawpoint)
	ON_BN_CLICKED(IDC_POINTLINE, &CdrawLIHEDlg::OnBnClickedPointline)
	ON_BN_CLICKED(IDC_BUTTON4, &CdrawLIHEDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_POINTFIT, &CdrawLIHEDlg::OnBnClickedPointfit)
END_MESSAGE_MAP()


// CdrawLIHEDlg message handlers

BOOL CdrawLIHEDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CdrawLIHEDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CdrawLIHEDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CdrawLIHEDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CdrawLIHEDlg::DrawPoint(double x, double y)
{

	CRect PicRect;
	m_pic.GetClientRect(&PicRect);
	double wide  = PicRect.right;
	double high = PicRect.bottom;


	CRect CircleRect;
	CircleRect.left = x * wide / Xmax - 2;
	CircleRect.right = x * wide / Xmax + 2;
	CircleRect.top = high - y * high / Ymax - 2;
	CircleRect.bottom = high - y * high / Ymax + 2;


	CDC * pDC = m_pic.GetDC();
	CBrush brush(RGB(0,0,255));   
	CBrush *old = pDC->SelectObject(&brush);   
	pDC->Ellipse(CircleRect);   
	pDC->SelectObject(old);
	

}

void CdrawLIHEDlg::DrawLine(double xstart, double ystart, double xend, double yend)
{

	CRect PicRect;
	m_pic.GetClientRect(&PicRect);
	double wide  = PicRect.right;
	double high = PicRect.bottom;

	double X0 = xstart * wide / Xmax;
	double Y0 = high - ystart * high / Ymax;
	double X1 = xend * wide / Xmax;
	double Y1 = high- yend * high / Ymax;


	CDC * pDC = m_pic.GetDC();
	int penWidth = 1;
	CPen pen(PS_SOLID, penWidth, RGB(128, 128, 225));
	pDC->SelectObject(&pen);  
	pDC->MoveTo(X0,Y0);
	pDC->LineTo(X1,Y1);

}



void CdrawLIHEDlg::OnBnClickedDrawpoint()
{
	// TODO: Add your control notification handler code here
	for(int i= 1;i<=DataCount;i++)
	{	
		DrawPoint(Xdata[i-1],Ydata[i-1]);
	}
}

void CdrawLIHEDlg::OnBnClickedPointline()
{
	// TODO: Add your control notification handler code here
	double Xstart = 0;
	double Ystart = 0;
	for(int i= 1;i<=DataCount;i++)
	{	
		DrawLine(Xstart,Ystart,Xdata[i-1],Ydata[i-1]);
		Xstart = Xdata[i-1];
		Ystart = Ydata[i-1];

	}

}

void CdrawLIHEDlg::OnBnClickedButton4()
{
	// TODO: Add your control notification handler code here
	ClearPic();
}

void CdrawLIHEDlg::ClearPic()
{
	// TODO: Add your control notification handler code here
	CRect PicRect;
	m_pic.GetClientRect(&PicRect);
	CDC * pDC = m_pic.GetDC();
	CBrush brush(RGB(255,255,255));   
	CBrush *old = pDC->SelectObject(&brush); 
	pDC->Rectangle(PicRect);
	pDC->SelectObject(old);
}
void CdrawLIHEDlg::DrawLineFx(double k ,double b)
{
	double x0 = 0;
	double y0  =   k * x0 + b;

	double x1 = Xmax;
	double y1 = k * x1 + b;

	DrawLine(x0,y0,x1,y1);
}

//最小二乘法直线拟合
BOOL CdrawLIHEDlg::CalculateLineKB(double *x,double *y,int n,double &k,double &b)
{
	//最小二乘法直线拟合
	//X为采集数据保存的X的值
	//Y为采集数据保存的Y的值
	//n为采集数据的数量
	//拟合直线方程(Y=kX+b)
	if(n < 2)
		return FALSE;

	double mX,mY,mXX,mXY;
	mX=mY=mXX=mXY=0;
	for(int i = 1; i<= n; i++) 
	{
		mX += x[i-1];
		mY += y[i-1];
		mXX += x[i-1] * x[i-1];
		mXY += x[i-1] * y[i-1];
	}
	if(mX*mX-mXX*n == 0)
		return FALSE;
	k = (mY*mX-mXY*n)/(mX*mX-mXX*n);
	b = (mY-mX*k)/n;
	return TRUE;
}


void CdrawLIHEDlg::OnBnClickedPointfit()
{
	// TODO: Add your control notification handler code here

	double k ,b;
	CalculateLineKB(Xdata,Ydata,DataCount,k,b);

	DrawLineFx(k,b);

}
